# Fedops' Scripts

A collection of scripts that make life easier, especially in a tiling window
manager environment.

Use at your own risk!

# Installation

Clone this repo, examine the contents, and copy whatever you like into your
`~/bin` directory or any other convenient place that is in your path.

Note that in most cases you will have to have dependencies installed. For
example the dmenu_* scripts require either `dmenu` or `rofi` to be installed.
I've made an effort to call this out in the comments at the top of each script.

# License

All content is licensed under GPLv3.
