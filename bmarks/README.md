# bmarks resources

The CSS and top and bottom HTML fragments for the bmarks bookmarks manager script
described in [The Personal Landing Page](https://fedops.codeberg.page/the-personal-landing-page.html).

You will have to adapt them to your own liking.

Add more static links to the ones left in `top.html`.

In `bottom.html` search for "YOUR" and insert your own keys for CheckWX and Open
Weather Map, or substitute them with the services you like.

The CSS uses Google fonts; replace with your choice if required.

# License

All content is licensed under GPLv3.
