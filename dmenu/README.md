# DMenu Scripts

These scripts actually use `rofi` since I run Wayland. They should function
equally well with dmenu itself or any of the other clones, potentially with some
massaging of the parameters.

## dmenu_keys

A handy keybinding reference, specific to sway. Parses the sway config for
`bindsym` keybindings and displays them. Selecting one of them will execute the
command. Useful for memorizing seldom-used keys in tiling window manager
environments.

![searching for dmenu keybindings](../images/keys.png)

## dmenu_calc

A minimal calculator inside a rofi menu. Result is displayed in rofi and can
also be copied to the clipboard.

![result of 4*11 calculation](../images/44.png)

## dmenu_emoji

An emoji picker. It reads the emoji file from `~/bin/emoji`. It is extremely
generic and can be used for anything else simply by providing a different file
to read the entries from.

![emoji picker in action](../images/emojis.png)

## dmenu_sound

If you have multiple audio devices attached, such as a monitor with loudspeakers
or a USB DAC, this script can be used to switch audio outputs on the fly. It
will list the given audio sinks, indicating the currently active one, and upon
user choice will switch to a different one and play a short jingle to indicate
things are working.

![sound sink picker in action](../images/sound.png)

It makes use of the Puleseaudio utilities, specifically `pactl`. If you run
Pipewire things should work as long as you also have a Pulseaudio server part
(tested on Fedora 35).

# Rofi configs

The configs subdirectory has the configuration files for the flat orange theme.
